#include "dsh.h"
//#include "helper.c"

void seize_tty(pid_t callingprocess_pgid); /* Grab control of the terminal for the calling process pgid.  */
void continue_job(job_t *j); /* resume a stopped job */
void spawn_job(job_t *j, bool fg); /* spawn a new job */

bool execute_jobs(job_t* job); /* Executes parsed in jobs */
void print_jobs();


int update_process_status(pid_t pid, int status);
void update_status(job_t* j);
void wait_on_job(job_t* j);
void format_job_info (job_t* j, char* status);
void check_jobs();
void add_to_jobs(job_t* j) ;
char buffer[30];

job_t* jobs = NULL;

/* Sets the process group id for a given job and process */
int set_child_pgid(job_t *j, process_t *p)
{
    if (j->pgid < 0) /* first child: use its pid for job pgid */
        j->pgid = p->pid;
    return(setpgid(p->pid,j->pgid));
}

/* Creates the context for a new child by setting the pid, pgid and tcsetpgrp */
void new_child(job_t *j, process_t *p, bool fg)
{
         /* establish a new process group, and put the child in
          * foreground if requested
          */

         /* Put the process into the process group and give the process
          * group the terminal, if appropriate.  This has to be done both by
          * the dsh and in the individual child processes because of
          * potential race conditions.  
          * */

         p->pid = getpid();

         /* also establish child process group in child to avoid race (if parent has not done it yet). */
         set_child_pgid(j, p);

         if(fg) // if fg is set
		seize_tty(j->pgid); // assign the terminal

         /* Set the handling for job control signals back to the default. */
         signal(SIGTTOU, SIG_DFL);

}

/* Spawning a process with job control. fg is true if the 
 * newly-created process is to be placed in the foreground. 
 * (This implicitly puts the calling process in the background, 
 * so watch out for tty I/O after doing this.) pgid is -1 to 
 * create a new job, in which case the returned pid is also the 
 * pgid of the new job.  Else pgid specifies an existing job's 
 * pgid: this feature is used to start the second or 
 * subsequent processes in a pipeline.
 * */

void spawn_job(job_t *j, bool fg) 
{
	pid_t pid;
	process_t *p;
    int prev_pipe[2];

    /* Iterate Over Job's Processes */
	for(p = j->first_process; p; p = p->next) {
        /* Initialize Piping for Current Process  */
        int pipe_file_desc[2];  
        if(pipe(pipe_file_desc) < 0) {
            perror("pipe");
            return;
        }

        /* Open Redirction Files */
        // Open output file, or create it if it doesn't exist
        int output_file_desc = -1; 
        if(p->ofile != NULL) {
            output_file_desc = open(p->ofile, O_WRONLY|O_TRUNC|O_CREAT, 0644);
            if(output_file_desc < 0) {
                perror("Error");
                return;
            }
        }
        // Open input file
        int input_file_desc = -1;
        if(p->ifile != NULL){ 
            input_file_desc = open(p->ifile, O_RDONLY);
            // Return if file specified is invalid, or
            // if it can't be opened
            if(input_file_desc < 0) { 
                perror("Error");
                return;
            }
        } 

        /* Fork the Parent Process */
	    switch (pid = fork()) {
            case -1:
                perror("fork");
                exit(EXIT_FAILURE);
            case 0:
                // Setup forked child PID
                p->pid = getpid();
                new_child(j, p, fg);
                printf("%s pid: %d\n", p->argv[0], p->pid);
                /* Pipe Handling */
                // First process has no pipe in
                if(p != j->first_process) {
                    close(prev_pipe[1]);
                    if(dup2(prev_pipe[0], STDIN_FILENO) < 0) perror("dup2 in");
                    close(prev_pipe[0]);
                }
                // Pipe a middle process
                if(p->next != NULL) {
                    close(pipe_file_desc[0]);
                    if(dup2(pipe_file_desc[1], STDOUT_FILENO) < 0) perror("dup2 out");
                    close(pipe_file_desc[1]);
                } 
                // End of the pipe chain 
                else {
                    int i = dup2(STDOUT_FILENO, pipe_file_desc[1]);
                    close(pipe_file_desc[0]);
                    close(pipe_file_desc[1]);
                }

                /* Input and Output Redirection */
                // Output file specified
                if(p->ofile != NULL) {  
                    if(dup2(output_file_desc, STDOUT_FILENO) < 0) perror("dup2");
                    close(output_file_desc);
                }
                // Input file specified
                if(p->ifile != NULL) {
                    if(dup2(input_file_desc, STDIN_FILENO) < 0) perror("dup2");
                    close(input_file_desc);
                }

                // Executes the child process
                execvp(p->argv[0], p->argv);

                // The code below here should only be reached due to an error 
                perror("New child should have done an exec");
                exit(EXIT_FAILURE);
                break;
            default:
                // Establish child process group 
                p->pid = pid;
                set_child_pgid(j, p);
                printf("%d(Launched): %s\n", pid, p->argv[0]);

                // Close redirection files
                if(p->ofile != NULL) close(output_file_desc); 
                if(p->ifile != NULL) close(input_file_desc);

                // Adjust last pipe output file to be the current pipe
                prev_pipe[0] = pipe_file_desc[0];
                prev_pipe[1] = pipe_file_desc[1];
        }
        // Close last pipe
        if(p->next == NULL) close(pipe_file_desc[0]);
        close(prev_pipe[1]);
        if(fg) {
            wait_on_job(j);
        } 

            seize_tty(getpid());
	}
        for(p = j->first_process; p; p = p->next) {
            printf("%s | PID: %d\n", p->argv[0], p->pid);
        }
        /* Handle FG/BG Specification */
        
}

int update_process_status(pid_t pid, int status)
{
    job_t* j;
    process_t* p;
    if (pid > 0) {
        for(j = jobs; j; j = j->next) {
            for(p = j->first_process; p; p = p->next) {
                printf("%s | Looking for PID: %d | PID: %d | PGID: %d\n", p->argv[0], pid, p->pid, j->pgid);
                if(p->pid == pid) {
                    p->status = status;
                    if(WIFSTOPPED(status)) {
                        p->stopped = 1;
                    }
                    else {
                        p->completed = 1;
                        if(WIFSIGNALED(status)) {
                            fprintf (stderr, "%d: Terminated by signal %d.\n",
                             (int) pid, WTERMSIG (p->status));
                        }
                    }
                    return 0;
                }
                return -1;    
            }
        }
    }
    else if (pid == 0 || errno == ECHILD) {
        return -1;
    } 
    else {
        perror("waitpid");
        return -1;
    }
}

/* Non-blocking */
void update_status(job_t* j) 
{
    int status;
    pid_t pid;
    do {
        pid = waitpid(WAIT_ANY, &status, WUNTRACED | WNOHANG);
    } while (!update_process_status(pid,status));
}

/* Blocking */
void wait_on_job(job_t* j)
{
    int status; 
    pid_t pid; 
    do {
        pid = waitpid(0, &status, WUNTRACED);
    } while (!update_process_status(pid, status) && !job_is_stopped(j) && !job_is_completed(j));
}


/* Sends SIGCONT signal to wake up the blocked job */
void continue_job(job_t *j) 
{
     if(kill(-j->pgid, SIGCONT) < 0)
          perror("kill(SIGCONT)");
}


/* 
 * builtin_cmd - If the user has typed a built-in command then execute
 * it immediately.  
 */
bool builtin_cmd(job_t *last_job, int argc, char **argv) 
{

	/* check whether the cmd is a built in command */
    if (!strcmp(argv[0], "quit")) {
        exit(EXIT_SUCCESS);
	}
    else if (!strcmp("jobs", argv[0])) {
        print_jobs();
        return true;
    }
	else if (!strcmp("cd", argv[0])) {
        if(chdir(argv[1]) == -1) {
            perror("Error");
        }
        return true;
    }
    else if (!strcmp("bg", argv[0])) {
        /* Your code here */
        return true;
    }
    else if (!strcmp("fg", argv[0])) {
        /* Your code here */
        return true;
    }
    // Command is not a built in command
    return false;       
}

void print_jobs() {
    //check_jobs();
    job_t* head = jobs; 
    while(head != NULL) {
        if(job_is_completed(head)) {
            fprintf (stderr, "%d (%s): %s\n", head->pgid, "Completed" , head->commandinfo);
        }
        else if(job_is_stopped(head)) {
            fprintf (stderr, "%d (%s): %s\n", head->pgid, "Stopped" , head->commandinfo);
        }
        else {
            fprintf (stderr, "%d (%s): %s\n", head->pgid, "Running" , head->commandinfo);
        }
        head = head->next;
    }
}

void format_job_info (job_t* j, char* status)
{
  fprintf (stderr, "%ld (%s): %s\n", (long)j->pgid, status, j->commandinfo);
}


void check_jobs() {
    job_t *j, *jlast, *jnext;

    /* Update status information for child processes.  */
    update_status (jobs);

    jlast = NULL;
    for (j = jobs; j; j = jnext)
        {
        jnext = j->next;

        /* If all processes have completed, tell the user the job has
            completed and delete it from the list of active jobs.  */
            process_t* p;
        for(p = j->first_process; p; p = p->next) {
            printf("PID: %d | Completed: %d\n", p->pid, p->completed);
        }
        if (job_is_completed (j)) {
            
            format_job_info (j, "completed");
            if (jlast) {
                jlast->next = jnext;
            } 
            else {
                jobs = jnext;
            }
            free_job(j);
        }

        /* Notify the user about stopped jobs,
            marking them so that we won’t do this more than once.  */
        else if (job_is_stopped (j) && !j->notified) {
            format_job_info (j, "stopped");
            j->notified = 1;
            jlast = j;
        }

        /* Don’t say anything about jobs that are still running.  */
        else
            jlast = j;
        }
}

/* Build prompt messaage */
char* promptmsg() 
{
    // Check background jobs before showing prompt
    check_jobs();
    fflush(stdout);
    snprintf(buffer, 30, "dsh-%d$ ", getpid());
    return buffer;
}

void add_to_jobs(job_t* j) 
{
    job_t* head = jobs;
    if(head == NULL) {
        jobs = j;
        return;
    }
    while(head->next != NULL) {
        head = head->next;
    }
    head->next = j;
}

bool execute_jobs(job_t* job)
{
    /**
     * Iterate over each individual job in the passed in job
     * list. For each job, iterate over that jobs respective
     * processes and execute them. Returns true if there are
     * no errors with the execution.
     */
    while(job != NULL) {
        if(!builtin_cmd(job, job->first_process->argc, job->first_process->argv)) {
            add_to_jobs(job);
                // Spawns the new job either in the background or the foreground   
            spawn_job(job, !(job->bg));
        }
        job = job->next; 
    }
    return true;
}

int main() 
{

	init_dsh();
	DEBUG("Successfully initialized\n");

	while(1) {
        // Read in job from the command line 
        job_t *j = NULL;
		if(!(j = readcmdline(promptmsg()))) {
			if(feof(stdin)) { // End of file character (ctrl-d) 
				fflush(stdout);
				printf("\n");
				exit(EXIT_SUCCESS);
           	}
			continue; // NOOP; user entered return or spaces with return 
		}

        // Prints job information for debugging purposes
        // if(PRINT_INFO) print_job(j);
    
        // Execute the read in jobs

        bool jobs_executed = execute_jobs(j);
        
        
        // Handle jobs execution failure
        if(!jobs_executed) {
            // TODO: Handle jobs failure appropriately
        }

        
    }
}
